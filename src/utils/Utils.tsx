enum ScreenNames {
  LOGIN = 'Login',
  SINGUP = 'Singup',
  MORE = 'More',
  PHOTOS = 'Photos',
  GALLERY = 'Gallery',
  TAB = 'Tab',
}

export class Utils {
  static readonly screenNames = ScreenNames;
  static readonly userKey = 'User';
}
