import {NavigationContainer} from '@react-navigation/native';

import * as React from 'react';
import {RootReducerState} from '../../reducer';
import {connect} from 'react-redux';
import {AuthStack, DrawerStack} from './Routes';
import {AsyncStorage} from 'react-native';
import {Utils} from '../Utils';
import {bindActionCreators} from 'redux';
import {UserActions} from '../../actions/User';
interface Props {
  isLoggedIn: boolean;
  UpdateUserAction: any;
}

interface State {
  isLoggedIn: boolean;
}

class Navigator extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    AsyncStorage.getItem(Utils.userKey).then(data => {
      if (data) {
        const isLoggedIn = JSON.parse(data).loggedIn;
        this.props.UpdateUserAction({isLoggedIn: isLoggedIn});
      }
    });
  }

  render() {
    return (
      <NavigationContainer>
        {this.props.isLoggedIn ? DrawerStack() : AuthStack()}
      </NavigationContainer>
    );
  }

  componentDidMount(): void {}
}

const mapStateToProps = (state: RootReducerState) => ({
  isLoggedIn: state.userReducer.isLoggedIn,
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      UpdateUserAction: UserActions.UpdateUserAction,
    },
    dispatch,
  );
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Navigator);
