import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Login from '../../screens/Login';
import * as React from 'react';
import Signup from '../../screens/Signup';
import {Utils} from '../Utils';
import More from '../../screens/More';
import {PhotoStack} from './tab-route/PhotoStack';
import Icon from 'react-native-vector-icons/Ionicons';
import SideMenu from '../../screens/SideMenu';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
export const AuthStack = () => {
  return (
    <Stack.Navigator initialRouteName={Utils.screenNames.LOGIN}>
      <Stack.Screen name={Utils.screenNames.LOGIN} component={Login} />
      <Stack.Screen name={Utils.screenNames.SINGUP} component={Signup} />
    </Stack.Navigator>
  );
};

const tabScreenOptions = ({route}) => {
  let isTabVisible = true;
  if (route.state && route.state.index > 0) {
    isTabVisible = false;
  }
  return {
    tabBarIcon: ({color}) => {
      const icons = {
        [Utils.screenNames.PHOTOS]: 'ios-photos',
        [Utils.screenNames.MORE]: 'ios-menu',
      };
      if (!icons[route.name]) {
        return;
      }
      return <Icon name={icons[route.name]} size={40} color={color} />;
    },
    tabBarVisible: isTabVisible,
  };
};

export const TabStack = () => {
  return (
    <Tab.Navigator
      screenOptions={tabScreenOptions}
      tabBarOptions={{inactiveTintColor: 'black', activeTintColor: 'orange'}}
      initialRouteName={Utils.screenNames.PHOTOS}>
      <Tab.Screen name={Utils.screenNames.PHOTOS} component={PhotoStack} />
      <Tab.Screen name={Utils.screenNames.MORE} component={More} />
    </Tab.Navigator>
  );
};

export const DrawerStack = () => {
  return (
    <Drawer.Navigator
      drawerContent={() => SideMenu}
      initialRouteName={Utils.screenNames.TAB}>
      <Drawer.Screen name={Utils.screenNames.TAB} component={TabStack} />
    </Drawer.Navigator>
  );
};
