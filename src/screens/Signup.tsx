import * as React from 'react';
import {Text} from 'react-native';
import Container from '../components/Container';

interface Props {}

interface State {}

class Signup extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <Text>This is my signUp Screen</Text>
      </Container>
    );
  }

  componentDidMount(): void {}
}

export default Signup;
