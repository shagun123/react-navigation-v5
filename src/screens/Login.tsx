import * as React from 'react';
import {Button, Text} from 'react-native';
import Container from '../components/Container';
import {Utils} from '../utils/Utils';
import {AsyncStorage} from 'react-native';
import {bindActionCreators} from 'redux';
import {UserActions} from '../actions/User';
import {connect} from 'react-redux';
interface Props {
  navigation: any;
  route: any;
  UpdateUserAction: any;
}

interface State {}

class Login extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleLogin = async () => {
    await AsyncStorage.setItem(Utils.userKey, JSON.stringify({loggedIn: true}));
    this.props.UpdateUserAction({isLoggedIn: true});
  };
  render() {
    return (
      <Container>
        <Text>This is my Login Page</Text>
        <Button title={'Login'} onPress={this.handleLogin} />
        <Button
          title={'Singup'}
          onPress={() =>
            this.props.navigation.navigate(Utils.screenNames.SINGUP)
          }
        />
      </Container>
    );
  }

  componentDidMount(): void {}
}

const mapDispatchToState = dispatch =>
  bindActionCreators(
    {
      UpdateUserAction: UserActions.UpdateUserAction,
    },
    dispatch,
  );
export default connect(
  null,
  mapDispatchToState,
)(Login);
